﻿using System;
using System.Collections.Generic;

namespace BlogCr.Models.Navigatorv4
{
    public partial class Avatar
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public int PicType { get; set; }
        public string ImgFormat { get; set; } = null!;
        public string Section { get; set; } = null!;
        public string Subsection { get; set; } = null!;
        public string Unit { get; set; } = null!;
    }
}
