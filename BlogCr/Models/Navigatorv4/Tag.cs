﻿using System;
using System.Collections.Generic;

namespace BlogCr.Models.Navigatorv4
{
    public partial class Tag
    {
        public long Id { get; set; }
        public string TagName { get; set; } = null!;
    }
}
