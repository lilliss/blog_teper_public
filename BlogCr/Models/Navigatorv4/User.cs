﻿using System;
using System.Collections.Generic;

namespace BlogCr.Models.Navigatorv4
{
    public partial class User
    {
        public long Id { get; set; }
        public string Nickname { get; set; } = null!;
        public string Email { get; set; } = null!;
        public bool Confirmed { get; set; }
        public string Password { get; set; } = null!;
        public string RefreshToken { get; set; } = null!;
        public long CreatedAt { get; set; }
        public string ConfirmCode { get; set; } = null!;
    }
}
