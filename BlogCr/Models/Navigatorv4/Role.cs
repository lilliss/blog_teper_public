﻿using System;
using System.Collections.Generic;

namespace BlogCr.Models.Navigatorv4
{
    public partial class Role
    {
        public long Id { get; set; }
        public string RoleName { get; set; } = null!;
    }
}
