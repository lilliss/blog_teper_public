﻿using System;
using System.Collections.Generic;

namespace BlogCr.Models.Navigatorv4
{
    public partial class Picture
    {
        public long Id { get; set; }
        public long PostId { get; set; }
        public string ImgFormat { get; set; } = null!;
        public string Section { get; set; } = null!;
        public string Subsection { get; set; } = null!;
        public string Unit { get; set; } = null!;
    }
}
