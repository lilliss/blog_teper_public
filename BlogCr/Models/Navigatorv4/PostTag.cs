﻿using System;
using System.Collections.Generic;

namespace BlogCr.Models.Navigatorv4
{
    public partial class PostTag
    {
        public long Id { get; set; }
        public long PostId { get; set; }
        public long TagId { get; set; }
    }
}
