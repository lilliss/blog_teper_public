﻿using System;
using System.Collections.Generic;

namespace BlogCr.Models.Navigatorv4
{
    public partial class Post
    {
        public long Id { get; set; }
        public string Title { get; set; } = null!;
        public string Body { get; set; } = null!;
        public long CreatedAt { get; set; }
        public long UserId { get; set; }
    }
}
