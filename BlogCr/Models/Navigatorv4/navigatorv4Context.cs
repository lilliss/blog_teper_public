﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BlogCr.Models.Navigatorv4
{
    public partial class navigatorv4Context : DbContext
    {
        public navigatorv4Context()
        {
        }

        public navigatorv4Context(DbContextOptions<navigatorv4Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Avatar> Avatars { get; set; } = null!;
        public virtual DbSet<Comment> Comments { get; set; } = null!;
        public virtual DbSet<Picture> Pictures { get; set; } = null!;
        public virtual DbSet<Post> Posts { get; set; } = null!;
        public virtual DbSet<PostLike> PostLikes { get; set; } = null!;
        public virtual DbSet<PostTag> PostTags { get; set; } = null!;
        public virtual DbSet<PostView> PostViews { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<Subscriber> Subscribers { get; set; } = null!;
        public virtual DbSet<Tag> Tags { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<UserRole> UserRoles { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=10.2.223.78;port=3306;user=zebrain;password=df/754dd2sc&3Z;database=navigatorv4", Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.7.40-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");

            modelBuilder.Entity<Avatar>(entity =>
            {
                entity.ToTable("avatars");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("id");

                entity.Property(e => e.ImgFormat)
                    .HasMaxLength(45)
                    .HasColumnName("img_format");

                entity.Property(e => e.PicType)
                    .HasColumnType("int(11)")
                    .HasColumnName("pic_type");

                entity.Property(e => e.Section)
                    .HasMaxLength(45)
                    .HasColumnName("section");

                entity.Property(e => e.Subsection)
                    .HasMaxLength(45)
                    .HasColumnName("subsection");

                entity.Property(e => e.Unit)
                    .HasMaxLength(255)
                    .HasColumnName("unit");

                entity.Property(e => e.UserId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.ToTable("comments");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Body)
                    .HasMaxLength(1000)
                    .HasColumnName("body");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("created_at");

                entity.Property(e => e.PostId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("post_id");

                entity.Property(e => e.UserId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<Picture>(entity =>
            {
                entity.ToTable("pictures");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("id");

                entity.Property(e => e.ImgFormat)
                    .HasMaxLength(45)
                    .HasColumnName("img_format");

                entity.Property(e => e.PostId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("post_id");

                entity.Property(e => e.Section)
                    .HasMaxLength(45)
                    .HasColumnName("section");

                entity.Property(e => e.Subsection)
                    .HasMaxLength(45)
                    .HasColumnName("subsection");

                entity.Property(e => e.Unit)
                    .HasMaxLength(225)
                    .HasColumnName("unit");
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.ToTable("posts");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("id");

                entity.Property(e => e.Body)
                    .HasMaxLength(1000)
                    .HasColumnName("body");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("created_at");

                entity.Property(e => e.Title)
                    .HasMaxLength(45)
                    .HasColumnName("title");

                entity.Property(e => e.UserId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<PostLike>(entity =>
            {
                entity.ToTable("post_likes");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.PostId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("post_id");

                entity.Property(e => e.UserId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<PostTag>(entity =>
            {
                entity.ToTable("post_tag");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.PostId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("post_id");

                entity.Property(e => e.TagId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("tag_id");
            });

            modelBuilder.Entity<PostView>(entity =>
            {
                entity.ToTable("post_views");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.PostId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("post_id");

                entity.Property(e => e.UserId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.RoleName)
                    .HasMaxLength(100)
                    .HasColumnName("role_name");
            });

            modelBuilder.Entity<Subscriber>(entity =>
            {
                entity.ToTable("subscribers");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.SubscriberId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("subscriber_id");

                entity.Property(e => e.UserId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.ToTable("tags");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.TagName)
                    .HasMaxLength(45)
                    .HasColumnName("tag_name");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("id");

                entity.Property(e => e.ConfirmCode)
                    .HasMaxLength(6)
                    .HasColumnName("confirm_code");

                entity.Property(e => e.Confirmed).HasColumnName("confirmed");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("created_at");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.Nickname)
                    .HasMaxLength(100)
                    .HasColumnName("nickname");

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .HasColumnName("password");

                entity.Property(e => e.RefreshToken)
                    .HasMaxLength(255)
                    .HasColumnName("refresh_token");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.ToTable("user_role");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(10)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.RoleId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("role_id");

                entity.Property(e => e.UserId)
                    .HasColumnType("bigint(10)")
                    .HasColumnName("user_id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
