using Microsoft.AspNetCore.Mvc;

namespace BlogCr.Models
{
    /// <summary>
    /// ������ ��������� ������
    /// </summary>
    public class GetTokenModel
    {
        /// <summary>
        /// Email ������������
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// ������ ������������
        /// </summary>
        public string Password { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class RegisterNewExternalUserModel : GetTokenModel
    {
        /// <summary>
        /// ��� ������������
        /// </summary>
        public string Nickname { get; set; }

    }


    /// <summary>
    /// ����� ��������� ������
    /// </summary>
    public class AuthResult
    {
        /// <summary>
        /// token
        /// </summary>
        public string token { get; set; } = "";
        /// <summary>
        /// Refresh_token
        /// </summary>
        public string refresh_token { get; set; } = "";
        /// <summary>
        /// ���������
        /// </summary>
        public string message { get; set; } = "";
        /// <summary>
        /// True - ���� ������
        /// </summary>
        public bool hasSomeErrors { get; set; } = false;
    }


    /// <summary>
    /// ����� �������� ��������������
    /// </summary>
    public class AuthResultModel
    {
        /// <summary>
        /// ������ �� �������������� �������
        /// </summary>
        public bool isAuth { get; set; } = false;
        /// <summary>
        /// ���������
        /// </summary>
        public string message { get; set; } = "";
    }


    /// <summary>
    /// ������ ������������� ������� ������
    /// </summary>
    public class ConfirmModel
    {
        /// <summary>
        /// �����
        /// </summary>
        public string Email { get; set; } = "";
        /// <summary>
        /// ���
        /// </summary>
        public string Code { get; set; } = "";
    }
    

    /// <summary>
    /// 
    /// </summary>
    public class UserData
    {
        /// <summary>
        /// Id ������� ������
        /// </summary>
        public long Id { get; set; } = 0;
        /// <summary>
        /// ���
        /// </summary>
        public string Email { get; set; } = "";

        /// <summary>
        /// ���
        /// </summary>
        public string Nickname { get; set; } = "";

        /// <summary>
        /// ���� ��������
        /// </summary>
        public long CreatedAt { get; set; } = 0;

        /// <summary>
        /// ������������ ����������
        /// </summary>
        public string ImageUrl { get; set; } = "";
    }


    /// <summary>
    /// ������ Id ������������
    /// </summary>
    public class UserIds
    {
        /// <summary>
        /// Id ������������
        /// </summary>
        public long UserId { get; set; } = -1;
        /// <summary>
        /// ����� ������������
        /// </summary>
        public string Email { get; set; } = "";
    }


    /// <summary>
    /// ������ �������� �����
    /// </summary>
    public class CreatePostModel
    {
        /// <summary>
        /// ��������� �����
        /// </summary>
        public string title { get; set; } = "";
        /// <summary>
        /// ���� �����
        /// </summary>
        public string body { get; set; } = "";
    }


    /// <summary>
    /// ������ �����
    /// </summary>
    public class PostModel
    {
        /// <summary>
        /// Id �����
        /// </summary>
        public long Id { get; set; } = 0;
        /// <summary>
        /// ��������� �����
        /// </summary>
        public string title { get; set; } = "";
        /// <summary>
        /// ���� �����
        /// </summary>
        public string body { get; set; } = "";
        /// <summary>
        /// ���� �������� �����
        /// </summary>
        public long createdAt { get; set; } = 0;
        /// <summary>
        /// url �����������
        /// </summary>
        public List<string> imageUrls { get; set; } = new List<string>();
    }


    /// <summary>
    /// ������ �����������
    /// </summary>
    public class CreateComment
    {
        /// <summary>
        /// ���� �����������
        /// </summary>
        public string body { get; set; } = "";
    }
}