﻿namespace BlogCr
{
    public static class Configs
    {
        #region Роли пользователей
        /// <summary>
        /// Роль обычного пользователя
        /// </summary>
        public const string UserRoleUser = "user";
        /// <summary>
        /// Роль для смены токена
        /// </summary>
        public const string UserRoleRefresh = "refresh";
        /// <summary>
        /// Роль админа
        /// </summary>
        public const string UserRoleAdmin = "admin";
        #endregion

        #region Paths
        /// <summary>
        /// 
        /// </summary>
        public const string RootMediaPath = "C:\\Meida\\";
        /// <summary>
        /// 
        /// </summary>
        public const string AvatarPath = RootMediaPath + "avt\\";
        /// <summary>
        /// 
        /// </summary>
        public const string PostPath = RootMediaPath + "posts\\";
        #endregion
    }
}
