using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using MimeKit;
using MailKit.Net.Smtp;
using System.Net;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Drawing;
using BlogCr.Classes;
using BlogCr.Models;

namespace BlogCr.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AccessController : ControllerBase
    {
        private readonly ILogger<AccessController> _logger;

        public AccessController(ILogger<AccessController> logger)
        {
            _logger = logger;
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetTokenJson([FromBody] GetTokenModel model)
        {
            try
            {
                AuthResult token = GetTokenJsonP(model).Result;

                if (token.hasSomeErrors)
                {
                    return Unauthorized(new { message = token.message });
                }
                else
                {
                    return Ok(new
                    {
                        access_token = token.token,
                        refresh_token = token.refresh_token
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {model.Email}");
                return Problem();
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> RegisterNewUser([FromBody][Required] RegisterNewExternalUserModel model)
        {
            try
            {
                AuthResult token = RegisterNewUserP(model).Result;

                if (token.hasSomeErrors)
                {
                    return Unauthorized(new { message = token.message });
                }
                else
                {
                    return Ok(new
                    {
                        access_token = token.token,
                        refresh_token = token.refresh_token
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {model.Nickname}");
                return Problem();
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> CheckCodeForConfirmAccount([FromBody][Required] ConfirmModel model)
        {
            try
            {
                var result = checkCodeForConfirmAccountP(model.Email, model.Code).Result;

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {model.Email}");
                return Problem();
            }

            return Ok();
        }


        #region ��������������� ������
        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="useremail"></param>
        /// <returns></returns>
        private async Task<AuthResult> GetTokenJsonP(GetTokenModel model)
        {
            var result = new AuthResult();

            //AuthResultModel isAuth = new AuthResultModel();

            var isAuth = AuthUser(model.Email, model.Password);

            if (isAuth.isAuth)
            {
                result = GenerateTokenP(model.Email).Result;
            }

                return result;
        }


        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="useremail"></param>
        /// <returns></returns>
        private async Task<AuthResult> GenerateTokenP(string useremail)
        {
            #region ������������ �������� ������

            var identity = GetIdentity(useremail);

            var now = DateTime.UtcNow;

            var to = DateTime.UtcNow.AddYears(AuthOptions.LIFETIME);
            var to_refresh = DateTime.UtcNow.AddYears(AuthOptions.LIFETIME);



            // ������� JWT-�����
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    notBefore: now,
                    audience: AuthOptions.AUDIENCE,
                    claims: identity.Claims,
                    expires: to,
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            #endregion

            #region ������������ refresh-������
            var identity_refresh = GetIdentityForRefresh(useremail);

            // ������� refresh-�����
            var jwt_refresh = new JwtSecurityToken(
                        issuer: AuthOptions.ISSUER,
                        notBefore: now,
                        audience: AuthOptions.AUDIENCE,
                        claims: identity_refresh.Claims,
                        expires: to_refresh,
                        signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var encodedJwt_refresh = new JwtSecurityTokenHandler().WriteToken(jwt_refresh);
            #endregion

            var result = new AuthResult();

            result.hasSomeErrors = false;
            result.token = encodedJwt;
            result.refresh_token = encodedJwt_refresh;

            return result;
        }


        /// <summary>
        /// ����� �� ����������� ������������
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<AuthResult> RegisterNewUserP(RegisterNewExternalUserModel model)
        {
            var result = new AuthResult();

            if (isValidEmail(model.Email))
            {
                using (Models.Navigatorv4.navigatorv4Context dbContext = new Models.Navigatorv4.navigatorv4Context())
                {
                    var user = dbContext.Users
                               .Where(usr => usr.Email == model.Email)
                               .Count() == 0;

                    if (user)
                    {
                        string verifyCode = new SecretClass().GenereConfirmCode().Result;

                        var newUser = new Models.Navigatorv4.User()
                        {
                            Nickname = model.Nickname,
                            Email = model.Email,
                            Confirmed = false,
                            Password = new SecretClass().DecriptBlowfish(model.Password).Result,
                            CreatedAt = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                            RefreshToken = "",
                            ConfirmCode = verifyCode
                        };

                        dbContext.Users.Add(newUser);
                        await dbContext.SaveChangesAsync();

                        await sendCodeForConfirmAccount(newUser.Email, verifyCode);

                        result.hasSomeErrors = false;

                        return result;
                    }
                    else
                    {
                        result.hasSomeErrors = true;
                        result.message = UI_Texts.ALREADY_USED_EMAIL_WARNING;

                        return result;
                    }
                }
            }
            else
            {
                result.hasSomeErrors = true;
                result.message = UI_Texts.INCORRECT_EMAIL_WARNING;

                return result;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="useremail"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private async Task sendCodeForConfirmAccount(string useremail, string code)
        {
            using (Models.Navigatorv4.navigatorv4Context dbContext = new Models.Navigatorv4.navigatorv4Context())
            {
                var client = new SmtpClient();

                try
                {
                    var message = new MimeMessage();
                    message.From.Add(MailboxAddress.Parse("confirmaccountBlog@yandex.ru"));
                    message.To.Add(MailboxAddress.Parse(useremail));

                    var bodyBuilder = new BodyBuilder();
                    bodyBuilder.HtmlBody = "��� ��� ������������� ����������� " + code;
                    message.Subject = "������������� �����������";
                    message.Body = bodyBuilder.ToMessageBody();

                    await client.ConnectAsync("smtp.yandex.ru", 465, true);
                    await client.AuthenticateAsync(new NetworkCredential("confirmaccountBlog@yandex.ru", "vezvqfriprcjebqo")); // ������ �� �����: vezvqfriprcjebqo
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    client.Dispose();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="useremail"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private async Task<AuthResult> checkCodeForConfirmAccountP(string useremail, string code)
        {
            var result = new AuthResult();

            try
            {
                using (Models.Navigatorv4.navigatorv4Context dbContext = new Models.Navigatorv4.navigatorv4Context())
                {
                    var user = dbContext.Users
                               .Where(u => u.Email == useremail && u.ConfirmCode == code)
                               .Select(u => u)
                               .FirstOrDefault();

                    if (user != null)
                    {
                        if (user.ConfirmCode == code)
                        {
                            user.Confirmed = true;
                            user.ConfirmCode = "";

                            await dbContext.SaveChangesAsync();

                            result.hasSomeErrors = false;
                            return result;
                        }
                    }
                    else
                    {
                        result.hasSomeErrors = true;
                        result.message = UI_Texts.INCORRECT_VERIFY_CODE_WARNING;

                        return result;
                    }
                }

                return result;
            }

            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: UserEmail {useremail}");
                return result;
            }
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="useremail"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private static AuthResultModel AuthUser(string? useremail, string? password)
        {
            using (Models.Navigatorv4.navigatorv4Context dbContext = new Models.Navigatorv4.navigatorv4Context())
            {
                AuthResultModel result = new AuthResultModel();
                result.isAuth = false;
                bool isCorrectPassword = false;

                var UserData = dbContext.Users
                                   .Where(user => user.Email == useremail && user.Confirmed == true)
                                   .Select(user => user)
                                   .FirstOrDefault();

                if (UserData != null)
                {
                    if (UserData.Confirmed != false)
                    {
                        string secret = UserData.Password;

                        isCorrectPassword = new CheckSession().isCorrectPass(password, secret).Result;

                        if (isCorrectPassword)
                        {
                            result.isAuth = true;
                        }
                        else
                        {
                            result.message = UI_Texts.INCORRECT_PASSWORD_WARNING;
                        }
                    }
                }
                else
                {
                    result.message = UI_Texts.USER_NOT_FOUND_WARNING;
                }

                return result;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="useremail"></param>
        /// <returns></returns>
        private static ClaimsIdentity GetIdentity(string useremail)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, useremail));
            claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, Configs.UserRoleUser));

            using (Models.Navigatorv4.navigatorv4Context dbContext = new Models.Navigatorv4.navigatorv4Context())
            {



                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);

                return claimsIdentity;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="useremail"></param>
        /// <returns></returns>
        private static ClaimsIdentity GetIdentityForRefresh(string useremail)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, useremail));

            ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }


        /// <summary>
        /// �������� email �� ����������
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private static bool isValidEmail(string email)
        {
            string pattern = "^((([!#$%&'*+\\-/=?^_`{|}~\\w])|([!#$%&'*+\\-/=?^_`{|}~\\w][!#$%&'*+\\-/=?^_`{|}~\\.\\w]{0,}[!#$%&'*+\\-/=?^_`{|}~\\w]))[@]\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)$";
            return Regex.Match(email, pattern, RegexOptions.IgnoreCase).Success;
        }
        #endregion
    }
}