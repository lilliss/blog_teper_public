using BlogCr.Classes;
using BlogCr.Models;
using BlogCr.Models.Navigatorv4;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BlogCr.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class BlogsController : ControllerBase
    {
        private readonly ILogger<BlogsController> _logger;

        public BlogsController(ILogger<BlogsController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// �������� ������ �����
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<ActionResult> CreateNewPost([FromBody] CreatePostModel post)
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                var result = CreateNewPostP(userIds.UserId, post).Result;

                if (result)
                {
                    return Ok();
                }
                else
                {
                    return NoContent();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {User.Identity.Name}");
                return Problem();
            }
        }


        /// <summary>
        /// �������� ���� ������ ������������ �� ��� Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<ActionResult> GetPosts()
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                var result = GetPostsP(userIds.UserId).Result;

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: UserId {userIds.UserId}");
                return Problem();
            }
        }


        /// <summary>
        /// ���������� ����� � �����
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<ActionResult> AddLikeToPost([FromHeader(Name = "PostId")][Required] long PostId)
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                var result = AddLikeToPostP(userIds.UserId, PostId).Result;

                return Ok(new { LikeCount = result });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: UserId {userIds.UserId}");
                return Problem();
            }
        }



        [HttpPost]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<ActionResult> AddComment([FromHeader(Name = "PostId")][Required] long PostId,
                                                   [FromBody] CreateComment comment)
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                var result = AddCommentP(userIds.UserId, PostId, comment).Result;

                if (result)
                {
                    return Ok();
                }
                else
                {
                    return NoContent();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: UserId {userIds.UserId}");
                return Problem();
            }
        }



        #region ��������������� ������
        /// <summary>
        /// ����� �� �������� ������ �����
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="post"></param>
        /// <returns></returns>
        private async Task<bool> CreateNewPostP(long userId, CreatePostModel post)
        {
            using (navigatorv4Context dbContext = new navigatorv4Context())
            {
                var newPost = new Post();

                long date = DateTimeOffset.Now.ToUnixTimeMilliseconds();

                newPost.CreatedAt = date;
                newPost.Body = post.body;
                newPost.Title = post.title;
                newPost.UserId = userId;

                dbContext.Posts.Add(newPost);
                await dbContext.SaveChangesAsync();

                return true;
            }
        }


        /// <summary>
        /// ����� �� �������� ���� ������ ������������
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<List<PostModel>> GetPostsP(long userId)
        {
            var result = new List<PostModel>();

            using (navigatorv4Context dbContext = new navigatorv4Context())
            {
                result = dbContext.Posts
                         .Where(x => x.UserId == userId)
                         .Select(x => new PostModel() { body = x.Body, createdAt = x.CreatedAt, title = x.Title})
                         .ToList();

                return result;
            }
        }


        /// <summary>
        /// ����� �� ���������� ����� � �����
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns></returns>
        private async Task<int> AddLikeToPostP(long userId, long postId)
        {
            using (navigatorv4Context dbContext = new navigatorv4Context())
            {
                var checkPost = dbContext.Posts
                                    .Where(p => p.UserId == userId && p.Id == postId)
                                    .Select(p => p)
                                    .FirstOrDefault();

                if (checkPost != null)
                {
                    var checkLike = dbContext.PostLikes
                                    .Where(x => x.PostId == postId)
                                    .Select(x => x)
                                    .FirstOrDefault();

                    if (checkLike != null)
                    {
                        dbContext.PostLikes.Remove(checkLike);
                        await dbContext.SaveChangesAsync();
                    }
                    else
                    {
                        var newLike = new PostLike { PostId = postId, UserId = userId};

                        dbContext.PostLikes.Add(newLike);
                        await dbContext.SaveChangesAsync();
                    }

                }

                var countLike = dbContext.PostLikes
                                .Where(x => x.PostId == postId)
                                .Select(x => x)
                                .ToList().Count();

                return countLike;
            }
        }

        private async Task<bool> AddCommentP(long userId, long postId, CreateComment comm)
        {
            using (navigatorv4Context dbContext = new navigatorv4Context())
            {
                var checkPost = dbContext.Posts
                                    .Where(p => p.UserId == userId && p.Id == postId)
                                    .Select(p => p)
                                    .FirstOrDefault();

                if (checkPost != null)
                {
                    var newComment = new Comment();

                    long date = DateTimeOffset.Now.ToUnixTimeMilliseconds();

                    newComment.CreatedAt = date;
                    newComment.Body = comm.body;
                    newComment.UserId = userId;

                    dbContext.Comments.Add(newComment);
                    await dbContext.SaveChangesAsync();
                }

                return true;
            }
        }
        #endregion
    }
}