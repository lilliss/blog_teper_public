using BlogCr.Classes;
using BlogCr.Models;
using BlogCr.Models.Navigatorv4;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BlogCr.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<AccessController> _logger;

        public UsersController(ILogger<AccessController> logger)
        {
            _logger = logger;
        }


        /// <summary>
        /// ��������� ������ �� ������������
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<ActionResult> GetUserData()
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                var result = GetUserDataP(userIds.UserId).Result;

                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return NoContent();
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {User.Identity.Name}");
                return Problem();
            }
        }


        /// <summary>
        /// �������� �� ������
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<ActionResult> SubscribeToAuthor([FromHeader(Name = "AuthorId")] long userId)
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                var result = SubscribeToAuthorP(userIds.UserId).Result;

                if (result != null)
                {
                    return Ok(new { SubscribersCount = result });
                }
                else
                {
                    return NoContent();
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {User.Identity.Name}");
                return Problem();
            }
        }



        #region ��������������� ������
        /// <summary>
        /// ��������� ������ �� ������������
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<UserData?> GetUserDataP(long userId)
        {
            UserData result = null;

            using (navigatorv4Context dbContext = new navigatorv4Context())
            {
                var UserData = dbContext.Users
                                .Where(u => u.Id == userId)
                                .Select(u => u)
                                .FirstOrDefault();

                if (UserData != null)
                {
                    result = new UserData
                    {
                        CreatedAt = UserData.CreatedAt,
                        Email = UserData.Email,
                        Nickname = UserData.Nickname,
                        ImageUrl = "",
                        Id = UserData.Id
                    };
                }
            }

            return result;
        }


        /// <summary>
        /// ����� �� �������� �� ������
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<int> SubscribeToAuthorP(long userId)
        {
            using (navigatorv4Context dbContext = new navigatorv4Context())
            {
                var checkAuth = dbContext.Users
                                .Where(x => x.Id == userId)
                                .Select(x => x)
                                .FirstOrDefault();

                if (checkAuth != null)
                {
                    var subscribers = new Subscriber
                    {
                        SubscriberId = 0,
                        UserId = userId
                    };

                    dbContext.Subscribers.Add(subscribers);
                    await dbContext.SaveChangesAsync();
                }

                var countSub = dbContext.Subscribers
                                .Where(x => x.UserId == userId)
                                .Select(x => x)
                                .ToList().Count();

                return countSub;
            }
        }
        #endregion
    }
}