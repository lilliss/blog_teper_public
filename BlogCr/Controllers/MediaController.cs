using BlogCr.Classes;
using BlogCr.Models.Navigatorv4;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BlogCr.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class MediaController : ControllerBase
    {
        private readonly ILogger<MediaController> _logger;

        public MediaController(ILogger<MediaController> logger)
        {
            _logger = logger;
        }


        /// <summary>
        /// ����� �� �������� ������� �� ������
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<ActionResult> UploadProfileImage(IFormFile uploadedFile)
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                using (navigatorv4Context dbContext = new navigatorv4Context())
                {
                    if (Request.Form != null)
                    {
                        if (Request.Form.Files != null)
                        {
                            if (Request.Form.Files.Count == 1)
                            {
                                byte[] image = await GetByteArrayFromImageAsync(Request.Form.Files[0]);
                                string FileName = uploadedFile.FileName;
                                string Name = uploadedFile.Name;
                                string ContentType = uploadedFile.ContentType;

                                bool isSaved = await UploadProfileImageP(image, userIds.UserId, ContentType, dbContext);

                                return Ok(new { isUploaded = isSaved });
                            }
                        }
                    }

                    return NoContent();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {User.Identity.Name}");
                return Problem();
            }
        }


        /// <summary>
        /// ����� �� �������� �������� ����� �� ������
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<ActionResult> UploadPostImage([FromHeader(Name = "PostId")][Required] long PostId,   
                                                        List<IFormFile> uploadedFile)
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                bool isSaved = UploadPostManyImageP(uploadedFile, PostId, userIds.UserId).Result;

                return Ok(new { isUploaded = isSaved });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {User.Identity.Name}");
                return Problem();
            }
        }


        /// <summary>
        /// ����� �� �������� ������� �� ������
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Subsection"></param>
        /// <param name="Unit"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> DownloadProfilePhoto([FromQuery(Name = "Section")][Required] string Section,
                                                             [FromQuery(Name = "Subsection")][Required] string Subsection,
                                                             [FromQuery(Name = "Unit")][Required] string Unit)
        {
            #region ���� ������������, ����������� ������� ������
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                string contentPath = "C:\\Media\\avt";
                string file_path = "";

                if (Section == "default" && Subsection == "default")
                {
                    file_path += contentPath + "\\default.png";
                }
                else
                {
                    file_path += contentPath + "\\" + Section + "\\" + Subsection + "\\" + Unit + ".png";
                }

                if (System.IO.File.Exists(file_path))
                {
                    byte[] data = System.IO.File.ReadAllBytes(file_path);
                    string contentType = MimeMapping.MimeUtility.GetMimeMapping(file_path);

                    return File(data, contentType);
                }
                else
                {
                    file_path = "C:\\Media\\avt\\default.png";

                    if (System.IO.File.Exists(file_path))
                    {
                        byte[] data = System.IO.File.ReadAllBytes(file_path);
                        string contentType = MimeMapping.MimeUtility.GetMimeMapping(file_path);

                        return File(data, contentType);
                    }
                    else
                    {
                        return NoContent();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: UserId {userIds.UserId}");
                return Problem();
            }

        }


        /// <summary>
        /// ����� �� �������� �������� ����� �� ������
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Subsection"></param>
        /// <param name="Unit"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> DownloadPostPhoto([FromQuery(Name = "Section")][Required] string Section,
                                                          [FromQuery(Name = "Subsection")][Required] string Subsection,
                                                          [FromQuery(Name = "Unit")][Required] string Unit)
        {
            #region ���� ������������, ����������� ������� ������
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                string contentPath = "C:\\Media\\posts";
                string file_path = "";

                file_path += contentPath + "\\" + Section + "\\" + Subsection + "\\" + Unit + ".png";

                if (System.IO.File.Exists(file_path))
                {
                    byte[] data = System.IO.File.ReadAllBytes(file_path);
                    string contentType = MimeMapping.MimeUtility.GetMimeMapping(file_path);

                    return File(data, contentType);
                }
                else
                {
                    return NoContent();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: UserId {userIds.UserId}");
                return Problem();
            }

        }



        /// <summary>
        /// �������� ��������
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme, Roles = Configs.UserRoleUser)]
        public async Task<IActionResult> DeleteAvtPhoto([FromQuery(Name = "Section")][Required] string Section,
                                                        [FromQuery(Name = "Subsection")][Required] string Subsection,
                                                        [FromQuery(Name = "Unit")][Required] string Unit)
        {
            #region ���� ������������, ����������� ������� ������, �������� �� ���
            var userIds = new SecretClass().GetUserIdsByUsername(User.Identity.Name).Result;
            #endregion

            try
            {
                bool isDeleted = DeleteAvtPhotoP(userIds.UserId).Result;

                return Ok(new { isDelete = isDeleted });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, message: $"{ex.Message}, UserInfo: Username {User.Identity.Name}");
                return Problem();
            }
        }



        #region ��������������� ������
        /// <summary>
        /// ����� �������� ����� �� Request'�
        /// </summary>
        /// <param name="file">����</param>
        /// <returns></returns>
        private async Task<byte[]> GetByteArrayFromImageAsync(IFormFile file)
        {
            using (var target = new MemoryStream())
            {
                await file.CopyToAsync(target);
                return target.ToArray();
            }
        }


        /// <summary>
        /// ����� �� �������� ������� �� ������
        /// </summary>
        /// <param name="data"></param>
        /// <param name="userId"></param>
        /// <param name="ImageFormat"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        private async Task<bool> UploadProfileImageP(byte[] data, long userId, string ImageFormat, navigatorv4Context dbContext)
        {
            bool result = false;

            string ImageRootDirectory = "C:\\Media\\" + UI_Texts.mediaAvtFolderName + "\\";

            var AvatarData = dbContext.Avatars
                             .Where(img => img.UserId == userId)
                             .Select(img => img)
                             .ToList();

            if (AvatarData.Count == 0)
            {

                string Section = "1";
                string Subsection = userId.ToString();
                string Unit = await new SecretClass().GetHashSumToImge(data);
                bool isNeedToCreateSection = true;

                #region ���������� ��������� �����
                string[] Sections = Directory.GetDirectories(ImageRootDirectory);

                if (Sections.Length == 0)
                {
                    Directory.CreateDirectory(ImageRootDirectory + "\\" + Section);
                    Directory.CreateDirectory(ImageRootDirectory + "\\" + Section + "\\" + Subsection);
                }
                else
                {
                    bool isExistFreeSpace = false;

                    for (int i = 0; i < Sections.Length; i++)
                    {
                        int FileCount = Directory.GetDirectories(Sections[i]).Length;

                        if (FileCount < 50)
                        {
                            Section = Sections[i].TrimEnd('\\').Substring(Sections[i].LastIndexOf("\\") + 1);

                            if (!Directory.Exists(ImageRootDirectory + "\\" + Section + "\\" + Subsection))
                            {
                                Directory.CreateDirectory(ImageRootDirectory + "\\" + Section + "\\" + Subsection);
                            }

                            isExistFreeSpace = true;
                            break;
                        }
                    }

                    if (!isExistFreeSpace)
                    {
                        int maxNumber = 0;

                        for (int i = 0; i < Sections.Length; i++)
                        {
                            try
                            {
                                int SectionNumber = Convert.ToInt32(Sections[i].TrimEnd('\\').Substring(Sections[i].LastIndexOf("\\") + 1));

                                if (SectionNumber > maxNumber)
                                {
                                    maxNumber = SectionNumber;
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        maxNumber++;

                        Section = maxNumber.ToString();
                        Directory.CreateDirectory(ImageRootDirectory + "\\" + Section);
                        Directory.CreateDirectory(ImageRootDirectory + "\\" + Section + "\\" + Subsection);
                    }
                }
                #endregion

                #region �������� ����� � �����

                System.IO.File.WriteAllBytes(ImageRootDirectory + "\\" + Section + "\\" + Subsection + "\\" + Unit + ".png", data);
                #endregion

                #region �������� ������ � ��
                Avatar originalImage = new Avatar();

                originalImage.UserId = userId;
                originalImage.ImgFormat = ImageFormat;
                originalImage.Section = Section;
                originalImage.Subsection = Subsection;
                originalImage.Unit = Unit;

                dbContext.Avatars.Add(originalImage);
                await dbContext.SaveChangesAsync();

                result = true;
                #endregion
            }
            else 
            {
                string Unit = await new SecretClass().GetHashSumToImge(data);

                for (int i = 0; i < AvatarData.Count; i++)
                {
                    string FilePrefix = "_1";
                    byte[] FileData = data;

                    if (AvatarData[i].Unit.EndsWith("_0"))
                    {
                        FilePrefix = "_0";
                        FileData = await new CommonMethods().RotateAndResizeImage(data, 100, 100);
                        Unit = await new SecretClass().GetHashSumToImge(FileData);
                    }

                    if (Unit + FilePrefix != AvatarData[i].Unit)
                    {
                        // ������� �������� ������� �����
                        if (!Directory.Exists(ImageRootDirectory + "\\" + AvatarData[i].Section))
                        {
                            Directory.CreateDirectory(ImageRootDirectory + "\\" + AvatarData[i].Section);
                        }

                        if (!Directory.Exists(ImageRootDirectory + "\\" + AvatarData[i].Section + "\\" + AvatarData[i].Subsection))
                        {
                            Directory.CreateDirectory(ImageRootDirectory + "\\" + AvatarData[i].Section + "\\" + AvatarData[i].Subsection);
                        }

                        // ����� �������� ������� �����. � ���� ���� - ������

                        string FilePath = ImageRootDirectory + "\\" + AvatarData[i].Section + "\\" + AvatarData[i].Subsection + "\\" + AvatarData[i].Unit + ".png";

                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }

                        System.IO.File.WriteAllBytes(ImageRootDirectory + "\\" + AvatarData[i].Section + "\\" + AvatarData[i].Subsection + "\\" + Unit + FilePrefix + ".png", FileData);

                        AvatarData[i].Unit = Unit + FilePrefix;

                        await dbContext.SaveChangesAsync();

                        result = true;
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// ����� �� �������� �������� ����� �� ������
        /// </summary>
        /// <param name="data"></param>
        /// <param name="userId"></param>
        /// <param name="ImageFormat"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        private async Task<bool> UploadPostManyImageP(List<IFormFile> lstFiles, long postId, long userId)
        {
            bool result = false;

            string ImageRootDirectory = "C:\\Media\\" + UI_Texts.mediaPostsFolderName + "\\";

            using (navigatorv4Context dbContext = new navigatorv4Context())
            {
                // ������� ���� ���� ��  id ����� � id ������������
                // ����� ������� ���������� ��� ��������� �������� (< 5)
                // ��������� �������� ���, ����� �� ���� �� ������ 5

                int neededImageCount = 5;

                var checkPost = dbContext.Posts
                                .Where(p => p.UserId == userId && p.Id == postId)
                                .Select(p => p)
                                .FirstOrDefault();

                if (checkPost != null)
                {
                    var postImageCount = dbContext.Pictures
                                         .Where(pi => pi.PostId == checkPost.Id)
                                         .Select(pi => pi)
                                         .ToList().Count;

                    if (postImageCount <= neededImageCount)
                    {
                        neededImageCount = neededImageCount - postImageCount;
                    }

                    if (neededImageCount > lstFiles.Count)
                    {
                        neededImageCount = lstFiles.Count;
                    }

                    for (int i = 0; i < neededImageCount; i++)
                    {
                        byte[] data = await GetByteArrayFromImageAsync(lstFiles[i]);
                        string ImageFormat = lstFiles[i].ContentType;

                        var PostImageData = dbContext.Pictures
                                 .Where(img => img.PostId == postId)
                                 .Select(img => img)
                                 .ToList();

                        string Section = userId.ToString();
                        string Subsection = postId.ToString();
                        string Unit = await new SecretClass().GetHashSumToImge(data);

                        bool isNeedToCreateSection = true;

                        #region ���������� ��������� �����
                        string[] Sections = Directory.GetDirectories(ImageRootDirectory);

                        if (Sections.Length == 0)
                        {
                            Directory.CreateDirectory(ImageRootDirectory + "\\" + Section);
                            Directory.CreateDirectory(ImageRootDirectory + "\\" + Section + "\\" + Subsection);
                        }
                        else
                        {
                            bool isExistFreeSpace = false;

                            for (int k = 0; k < Sections.Length; k++)
                            {
                                int FileCount = Directory.GetDirectories(Sections[k]).Length;

                                if (FileCount < 50)
                                {
                                    Section = Sections[k].TrimEnd('\\').Substring(Sections[k].LastIndexOf("\\") + 1);

                                    if (!Directory.Exists(ImageRootDirectory + "\\" + Section + "\\" + Subsection))
                                    {
                                        Directory.CreateDirectory(ImageRootDirectory + "\\" + Section + "\\" + Subsection);
                                    }

                                    isExistFreeSpace = true;
                                    break;
                                }
                            }

                            if (!isExistFreeSpace)
                            {
                                int maxNumber = 0;

                                for (int j = 0; j < Sections.Length; j++)
                                {
                                    try
                                    {
                                        int SectionNumber = Convert.ToInt32(Sections[i].TrimEnd('\\').Substring(Sections[i].LastIndexOf("\\") + 1));

                                        if (SectionNumber > maxNumber)
                                        {
                                            maxNumber = SectionNumber;
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }

                                maxNumber++;

                                Section = maxNumber.ToString();
                                Directory.CreateDirectory(ImageRootDirectory + "\\" + Section);
                                Directory.CreateDirectory(ImageRootDirectory + "\\" + Section + "\\" + Subsection);
                            }
                        }
                        #endregion

                        #region �������� ����� � �����
                        System.IO.File.WriteAllBytes(ImageRootDirectory + "\\" + Section + "\\" + Subsection + "\\" + Unit + "_1.png", data);
                        #endregion

                        #region �������� ������ � ��
                        Picture originalImage = new Picture();

                        originalImage.PostId = postId;
                        originalImage.ImgFormat = ImageFormat;
                        originalImage.Section = Section;
                        originalImage.Subsection = Subsection;
                        originalImage.Unit = Unit;

                        dbContext.Pictures.Add(originalImage);
                        await dbContext.SaveChangesAsync();

                        result = true;
                        #endregion
                    }
                }
            }            

            return result;
        }


        /// <summary>
        /// ����� �� �������� ������� ������������
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<bool> DeleteAvtPhotoP(long userId)
        {
            bool result = false;

            string ImageRootDirectory = "C:\\Media\\" + UI_Texts.mediaAvtFolderName + "\\";

            using (navigatorv4Context dbContext = new navigatorv4Context())
            {
                var AvatarData = dbContext.Avatars
                                 .Where(img => img.UserId == userId)
                                 .Select(img => img)
                                 .FirstOrDefault();

                if (AvatarData != null)
                {
                    string FilePath = ImageRootDirectory + "\\" + AvatarData.Section + "\\" + AvatarData.Subsection + "\\" + AvatarData.Unit + ".png";

                    if (System.IO.File.Exists(FilePath))
                    {
                        System.IO.File.Delete(FilePath);
                    }

                    dbContext.Avatars.Remove(AvatarData);

                    await dbContext.SaveChangesAsync();

                    result = true;
                }
            }

            return result;
        }
        #endregion
    }
}