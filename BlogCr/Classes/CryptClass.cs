﻿using CryptSharp;

namespace BlogCr.Classes
{
    internal class CryptClass
    {
        public async Task<string> DecriptBlowfishWithSalt(string? value, string? salt)
        {
            string cryptedPassword = Crypter.Blowfish.Crypt(value, salt);

            return cryptedPassword;
        }
    }
}