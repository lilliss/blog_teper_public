﻿namespace BlogCr.Classes
{
    public class CheckSession
    {
        public async Task<bool> isCorrectPass(string? password, string? secret)
        {
            bool result = false;

            string salt = secret.Substring(0, 29);

            string checkPass = new CryptClass().DecriptBlowfishWithSalt(password, salt).Result;

            if (checkPass == secret)
            {
                result = true;
            }

            return result;
        }
    }
}
