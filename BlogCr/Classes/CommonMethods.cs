﻿using Microsoft.IdentityModel.Tokens;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;

namespace BlogCr.Classes
{
    public class CommonMethods
    {
        /// <summary>
        /// Метод возвращает уменьшенную картинку
        /// </summary>
        /// <param name="picture">Картинка</param>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</p
        public async Task<byte[]> MinimizePicture(Image picture, int width, int height)
        {
            byte[] result = new byte[] { 0 };

            Image original = picture;
            int x1 = width;
            int y1 = height;
            if (original.Width > original.Height)
            {
                x1 = width;
                y1 = (int)Math.Round((double)original.Height / (original.Width / height));

            }
            else
            {
                if (original.Width < original.Height)
                {
                    y1 = height;
                    x1 = (int)Math.Round((double)original.Width / (original.Height / width));
                }
            }

            Bitmap resized = new Bitmap(original, new Size(x1, y1));

            using (var ms = new MemoryStream())
            {
                resized.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                result = ms.ToArray();
            }

            return result;
        }



        /// <summary>
        /// Метод возвращает уменьшенную картинку
        /// </summary>
        /// <param name="picture">Картинка</param>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <returns></returns>
        public async Task<byte[]> RotateAndResizeImage(byte[] picture, int width, int height)
        {
            Bitmap imageFromPc = (Bitmap)Bitmap.FromStream(new MemoryStream(picture));

            const int OrientationKey = 0x0112;
            const int NotSpecified = 0;
            const int NormalOrientation = 1;
            const int MirrorHorizontal = 2;
            const int UpsideDown = 3;
            const int MirrorVertical = 4;
            const int MirrorHorizontalAndRotateRight = 5;
            const int RotateLeft = 6;
            const int MirorHorizontalAndRotateLeft = 7;
            const int RotateRight = 8;

            using (var newBitmap = new Bitmap((Image)imageFromPc))
            {
                using (var imageScaler = Graphics.FromImage(imageFromPc))
                {
                    imageScaler.CompositingQuality = CompositingQuality.HighQuality;
                    imageScaler.SmoothingMode = SmoothingMode.HighQuality;
                    imageScaler.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    var imageRectangle = new Rectangle(0, 0, imageFromPc.Width, imageFromPc.Height);
                    imageScaler.DrawImage(imageFromPc, imageRectangle);

                    // Fix orientation if needed.
                    if (imageFromPc.PropertyIdList.Contains(OrientationKey))
                    {
                        var orientation = (int)imageFromPc.GetPropertyItem(OrientationKey).Value[0];
                        switch (orientation)
                        {
                            case NotSpecified: // Assume it is good.
                                break;

                            case NormalOrientation:
                                // No rotation required.
                                break;

                            case MirrorHorizontal:
                                newBitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
                                break;

                            case UpsideDown:
                                newBitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                break;

                            case MirrorVertical:
                                newBitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
                                break;

                            case MirrorHorizontalAndRotateRight:
                                newBitmap.RotateFlip(RotateFlipType.Rotate90FlipX);
                                break;

                            case RotateLeft:
                                newBitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                break;

                            case MirorHorizontalAndRotateLeft:
                                newBitmap.RotateFlip(RotateFlipType.Rotate270FlipX);
                                break;

                            case RotateRight:
                                newBitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                break;

                            default:
                                break;
                        }
                    }
                }

                byte[] result = new CommonMethods().MinimizePicture(newBitmap, width, height).Result;

                return result;
            }
        }
    }
}
