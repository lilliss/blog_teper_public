﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace BlogCr.Classes
{
    public class AuthOptions
    {
        public const string ISSUER = "EcServices";           // издатель токена
        public const string AUDIENCE = "EcClientAndIBA";     // потребитель токена
        const string KEY = "ULSUSILAFTMOGILA";               // ключ шифрования
        public const int LIFETIME = 15;                      // время жизни токена - 1 минута
        public const int LIFETIMEREFRESH = 1;                // время жизни refresh-токена - 1 год
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
