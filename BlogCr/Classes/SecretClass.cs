﻿using CryptSharp;
using System.Security.Cryptography;
using System.Text;

namespace BlogCr.Classes
{
    public class SecretClass
    {
        /// <summary>
        /// Разделитель между 
        /// </summary>
        private const string delimeter = "del11data";

        /// <summary>
        /// Метод шифрует пароль пользователя
        /// </summary>
        /// <param name="value">Пароль</param>
        /// <returns></returns>
        public async Task<string> DecriptBlowfish(string value)
        {
            string salt = Crypter.Blowfish.GenerateSalt();
            string cryptedPassword = Crypter.Blowfish.Crypt(value, salt);

            return cryptedPassword;
        }

        private static List<string> symbols = new List<string>
        {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"
        };


        /// <summary>
        /// Метод генерации кода
        /// </summary>
        /// <returns></returns>
        public async Task<string> GenereConfirmCode()
        {
            Random rnd = new Random();

            string result = symbols[rnd.Next(0, 35)] + symbols[rnd.Next(0, 35)] + symbols[rnd.Next(0, 35)] + symbols[rnd.Next(0, 35)] + symbols[rnd.Next(0, 35)] + symbols[rnd.Next(0, 35)];

            return result;
        }

        public async Task<string> GenerateToken(long userId, string? nickname)
        {
            string result = "";

            result = userId.ToString() + delimeter + nickname;
            result = result.Replace("/", "$&");

            return result;
        }


        /// <summary>
        /// Метод возвращает тип источника даных в зависимости от 
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <returns></returns>
        public async Task<Models.UserIds> GetUserIdsByUsername(string username)
        {
            var result = new Models.UserIds();

            using (Models.Navigatorv4.navigatorv4Context dbContext = new Models.Navigatorv4.navigatorv4Context())
            {
                var UserData = dbContext.Users
                               .Where(x => x.Email == username)
                               .Select(x => x)
                               .FirstOrDefault();

                if (UserData != null)
                {
                    result.UserId = UserData.Id;
                    result.Email = UserData.Email;
                }
            }

            return result;
        }


        public async Task<string> GetHashSumToImge(byte[] Image)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] btImage = sha256Hash.ComputeHash(Image);
                var result = new StringBuilder();
                for (int p = 0; p < btImage.Length; p++)
                {
                    result.Append(btImage[p].ToString("x2"));
                }

                return result.ToString();
            }
        }
    }
}
