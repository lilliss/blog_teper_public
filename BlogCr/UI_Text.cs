namespace BlogCr
{
    public static class UI_Texts
    {
        /// <summary>
        /// �������
        /// </summary>
        public const string SUCCESS_MESSAGE = "�������!";

        /// <summary>
        /// ��������� Email �����������!
        /// </summary>
        public const string INCORRECT_EMAIL_WARNING = "��������� Email �����������!";

        /// <summary>
        /// ��������� Email �����!
        /// </summary>
        public const string ALREADY_USED_EMAIL_WARNING = "��������� Email �����!";

        /// <summary>
        /// ��� ������������� ���������!
        /// </summary>
        public const string SENDING_VERIFY_CODE_MESSAGE = "��� ������������� ���������!";

        /// <summary>
        /// ������������ �� ������!
        /// </summary>
        public const string USER_NOT_FOUND_WARNING = "������������ �� ������!";

        /// <summary>
        /// �������� ���!
        /// </summary>
        public const string INCORRECT_VERIFY_CODE_WARNING = "�������� ���!";

        /// <summary>
        /// �������� ������!
        /// </summary>
        public const string INCORRECT_PASSWORD_WARNING = "�������� ������!";

        /// <summary>
        /// ����� � ���������� �������������
        /// </summary>
        public const string mediaAvtFolderName = "avt";

        /// <summary>
        /// ����� ��� ����������� ����������
        /// </summary>
        public const string mediaPostsFolderName = "posts";

        /// <summary>
        /// ������
        /// </summary>
        public const string Host = "http://temp1.ulsu.ru/";
    }
}